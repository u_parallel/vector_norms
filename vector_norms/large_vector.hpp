#ifndef large_vector_hpp
#define large_vector_hpp

#include <vector>
#include <thread>
#include <algorithm>
#include <cmath>
#include <mutex>
#include <iostream>
#include <sstream>
#include <fstream>
#include "interval.hpp"



template <typename T>
class LargeVector {
public:
    LargeVector(ind_t n);
    LargeVector(std::string filename);
    LargeVector(ind_t n, unsigned int* indicator);
    LargeVector(std::string filename, unsigned int* indicator);
    ~LargeVector();
    void PrintVector();
    norm_t GetMaxNorm(ind_t workers_num = 2);
    norm_t GetEuqNorm(ind_t workers_num = 2);
    
private:
    void MaxNorm(ind_t s);
    void EuqNorm(ind_t s);
    void ThreadedNorm(void (LargeVector<T>::*Norm)(ind_t));
    norm_t MaxInResVec();
    norm_t SqSumResVec();
    void Split(ind_t parts);
    void Renew();
    void ProgressBar();
    void PutProgress(ind_t percent, ind_t stream_nmb);
    norm_t GetNorm(void (LargeVector<T>::*Norm)(ind_t),
                        norm_t (LargeVector<T>::*SubNorm)(),
                        ind_t workers_num);
    
    std::chrono::high_resolution_clock::time_point _start_time;
    IndicesStorage* _indices = new IndicesStorage();
    std::vector<norm_t>* _res = new std::vector<norm_t>;
    std::vector<T>* _vec = new std::vector<T>;
    std::recursive_mutex _lock;
    std::recursive_mutex _sub_norm_lock;
    std::vector<std::stringstream> _progress_bars;
    std::vector<std::mutex*> _progress_bars_mutexes;
    unsigned int* _process_ind = nullptr;
};


template <typename T>
LargeVector<T>::LargeVector(std::string filename, unsigned int* indicator) {
    _process_ind = indicator;
    std::fstream source;
    source.open(filename, std::fstream::in);
    if (source) {
        while (source) {
            T temp;
            source >> temp;
            _vec->push_back(temp);
        }
        _vec->pop_back();
        source.close();
    } else {
        std::string er = "bad file in ";
        er += __PRETTY_FUNCTION__;
        throw std::invalid_argument(er);
    }
}

/*
template <typename T>
LargeVector<T>::LargeVector(std::string filename, unsigned int* indicator) {
    _process_ind = indicator;
    std::fstream source;
    source.open(filename, std::fstream::in);
    char buf[1024 * 1024 * 1024];
    //ind_t read_amnt = 0;
    std::string row;
    if (source) {
        while (source) {
            source.read(buf, sizeof(buf));
            //read_amnt = source.gcount();
            row.append(buf);
            while (row.find_last_of(std::string("\n ")) != std::string::npos) {
                std::string::size_type sz;
                _vec->push_back((T)std::stold(row, &sz));
                row = row.substr(sz);
            }
            
        }
        _vec->push_back((T)std::stold(row));
        source.close();
    } else {
        std::string er = "bad file in ";
        er += __PRETTY_FUNCTION__;
        throw std::invalid_argument(er);
    }
}
*/

template <typename T>
LargeVector<T>::LargeVector(std::string filename) {
    std::fstream source;
    source.open(filename, std::fstream::in);
    if (source) {
        while (source) {
            T temp;
            source >> temp;
            _vec->push_back(temp);
        }
        _vec->pop_back();
        source.close();
    } else {
        std::string er = "bad file in ";
        er += __PRETTY_FUNCTION__;
        throw std::invalid_argument(er);
    }
}

template <typename T>
void LargeVector<T>::Renew() {
    delete _indices;
    _indices = new IndicesStorage();
    delete _res;
    _res = new std::vector<norm_t>;
    _progress_bars.clear();
    for (ind_t i = 0; i < _progress_bars_mutexes.size(); i++) {
        delete _progress_bars_mutexes[i];
    }
    _progress_bars_mutexes.clear();
}

template <typename T>
void LargeVector<T>::PutProgress(ind_t percent, ind_t stream_nmb) {
    if (percent > 100) {
        std::string er = "percent is greater than 100 in ";
        er += __PRETTY_FUNCTION__;
        throw std::invalid_argument(er);
    }
    if (stream_nmb > _progress_bars_mutexes.size()) {
        std::string er = "stream_nmb is out of range in ";
        er += __PRETTY_FUNCTION__;
        throw std::out_of_range(er);
    }
    _progress_bars_mutexes[stream_nmb]->lock();
    if (percent / 10 == 0) {
        _progress_bars[stream_nmb] << percent << "  ";
    } else if (percent / 100 == 0) {
        _progress_bars[stream_nmb] << percent << " ";
    } else if (percent == 100) {
        _progress_bars[stream_nmb] << percent;
    }
    _progress_bars_mutexes[stream_nmb]->unlock();
}

template <typename T>
LargeVector<T>::~LargeVector() {
    delete _vec;
    delete _indices;
    delete _res;
}

template <typename T>
norm_t LargeVector<T>::MaxInResVec() {
    norm_t max = 0;
    for (typename std::vector<norm_t>::iterator it = _res->begin(); it != _res->end(); it++) {
        if (*it > max) {
            max = *it;
        }
    }
    return max;
}

template <typename T>
norm_t LargeVector<T>::SqSumResVec() {
    norm_t norm = 0;
    for (typename std::vector<norm_t>::iterator it = _res->begin(); it != _res->end(); it++) {
        norm += *it;
    }
    return sqrt(norm);
}

template <typename T>
void LargeVector<T>::MaxNorm(ind_t s) {
    norm_t max = 0;
    Interval curr_indcs = Interval((*_indices)[s].GetStart(), (*_indices)[s].GetEnd());
    ind_t work = curr_indcs.GetSize();
    ind_t i = 0;
    ind_t curr_progress = 0;
    
    for (typename std::vector<T>::iterator it = _vec->begin() + curr_indcs.GetStart();
         it != _vec->end() && it != _vec->begin() + curr_indcs.GetEnd() + 1;
         it++) {
        
        //std::this_thread::sleep_for (std::chrono::seconds(1));
        
        norm_t cur_el = (norm_t)*it;
        if (cur_el < 0) {
            cur_el = -cur_el;
        }
        if (cur_el > max) {
            max = cur_el;
        }
        i++;
        if (i * 100 / work > curr_progress) {
            curr_progress = i * 100 / work;
            PutProgress(curr_progress, s);
        }
    }
    std::lock_guard<std::recursive_mutex> locker(_sub_norm_lock);
    _res->push_back(max);
    PutProgress(100, s);
}

template <typename T>
void LargeVector<T>::EuqNorm(ind_t s) {
    norm_t norm = 0;
    Interval curr_indcs = Interval((*_indices)[s].GetStart(), (*_indices)[s].GetEnd());
    ind_t i = 0;
    ind_t curr_progress = 0;
    ind_t work = curr_indcs.GetSize();
    for (typename std::vector<T>::iterator it = _vec->begin() + curr_indcs.GetStart();
         it != _vec->end() && it != _vec->begin() + curr_indcs.GetEnd() + 1;
         it++) {
        
        //std::this_thread::sleep_for (std::chrono::seconds(1));
        
        norm += (norm_t)*it * (norm_t)*it;
        i++;
        if (i * 100 / work > curr_progress) {
            curr_progress = i * 100 / work;
            PutProgress(curr_progress, s);
        }
    }
    
    std::lock_guard<std::recursive_mutex> locker(_sub_norm_lock);
    _res->push_back(norm);
    
    PutProgress(100, s);
}

template<>
LargeVector<norm_t>::LargeVector(ind_t n) {
    for (ind_t i = 0; i < n; i++) {
        _vec->push_back((norm_t)((i+1) % 10));
    }
}

template<>
LargeVector<unsigned long int>::LargeVector(ind_t n) {
    for (ind_t i = 0; i < n; i++) {
        _vec->push_back((i+1) % 10);
    }
}

template<>
LargeVector<norm_t>::LargeVector(ind_t n, unsigned int* indicator) {
    _process_ind = indicator;
    for (ind_t i = 0; i < n; i++) {
        _vec->push_back((norm_t)((i+1) % 10));
    }
}

template<>
LargeVector<unsigned long int>::LargeVector(ind_t n, unsigned int* indicator) {
    _process_ind = indicator;
    for (ind_t i = 0; i < n; i++) {
        _vec->push_back((i+1) % 10);
    }
}

template <typename T>
void LargeVector<T>::PrintVector() {
    for (typename std::vector<T>::iterator it = _vec->begin() ; it != _vec->end(); ++it) {
        std::cout << ' ' << *it;
    }
}

template <typename T>
void LargeVector<T>::ThreadedNorm(void (LargeVector<T>::*Norm)(ind_t)) {
    std::vector<std::thread*> threads;
    threads.push_back(new std::thread(&LargeVector::ProgressBar, this));
    for (unsigned long k = 0; k < _indices->GetSize(); k++) {
        threads.push_back(new std::thread(Norm, this, k));
    }
    
    for (unsigned long k = 0; k < threads.size(); k++) {
        threads[k]->join();
    }
}

template <typename T>
norm_t LargeVector<T>::GetNorm(void (LargeVector<T>::*Norm)(ind_t),
                                    norm_t (LargeVector<T>::*SubNorm)(),
                                    ind_t workers_num) {
    Split(workers_num);
    if (_indices->GetRecall(Interval(0, _vec->size() - 1)) == true) {
        _start_time = std::chrono::high_resolution_clock::now();
        ThreadedNorm(Norm);
        std::cout << "Overall time: " << (norm_t)(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - _start_time ).count()) / 1000000 << " seconds\n";
    } else {
        std::cout << "indices status is " << _indices->GetStatus() << " ERROR\n";
        return -1;
    }
    norm_t answ = (*this.*SubNorm)();
    Renew();
    return answ;
}

template <typename T>
void LargeVector<T>::Split(ind_t parts) {
    std::cout << "splitting " << _vec->size() << " to " << parts << '\n';
    
    try {
        _indices = new IndicesStorage(Interval(0, _vec->size() - 1), parts);
    } catch (std::bad_alloc& ba) {
        std::string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw std::runtime_error(er);
    }
    for (ind_t i = 0; i < _indices->GetSize(); i++) {
        _progress_bars.push_back(std::stringstream(std::ios::in | std::ios::out));
        _progress_bars[i] << 0 << "  ";
        _progress_bars_mutexes.push_back(new std::mutex());
    }
    std::cout << "success\n";
}

template <typename T>
norm_t LargeVector<T>::GetMaxNorm(ind_t workers_num) {
    return GetNorm(&LargeVector::MaxNorm, &LargeVector<T>::MaxInResVec, workers_num);
}

template <typename T>
norm_t LargeVector<T>::GetEuqNorm(ind_t workers_num) {
    return GetNorm(&LargeVector::EuqNorm, &LargeVector<T>::SqSumResVec, workers_num);
}

template <typename T>
void LargeVector<T>::ProgressBar() {
    std::vector<unsigned int> progress;
    bool done = false;
    for (ind_t i = 0; i < _progress_bars.size(); i++) {
        progress.push_back(0);
    }
    while (!done) {
        done = true;
        unsigned int overall_progress = 0;
        
        for (ind_t i = 0; i < progress.size(); i++) {
            if (progress[i] != 100) {
                done = false;
                std::cout << progress[i] << "% ";
            } else {
                std::cout << "end ";
            }
            if (progress.size() > 1) {
                overall_progress += progress[i];
            }
        }
        
        if (progress.size() > 1) {
            overall_progress /= progress.size();
            if (overall_progress != 100) {
                std::cout << "| " << overall_progress << "%";
            } else {
                std::cout << "| end";
            }
            if (_process_ind != nullptr) {
                *_process_ind = overall_progress;
            }
        } else {
            if (_process_ind != nullptr) {
                *_process_ind = progress[0];
            }
        }
        
        std::cout << '\n';
        
        for (ind_t i = 0; i < _progress_bars.size(); i++) {
            if (_progress_bars[i]) {
                _progress_bars_mutexes[i]->lock();
                _progress_bars[i].seekg(-3, std::ios::end);
                _progress_bars[i] >> progress[i];
                _progress_bars_mutexes[i]->unlock();
            }
        }
        std::this_thread::sleep_for (std::chrono::seconds(1));
    }
    
    if (_process_ind != nullptr) {
        *_process_ind = 100;
    }
    
}


#endif /* large_vector_hpp */
