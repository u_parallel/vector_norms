#ifndef interval_hpp
#define interval_hpp

#include <stdio.h>
#include <string>
#include <stdexcept>
#include <vector>

typedef long double norm_t;
typedef unsigned long long int ind_t;

enum indices_status {
    UNKNOWN,
    OK,
    BAD,
};

/** Class to store the interval for one worker */
class Interval {
public:
    Interval() {};
    Interval(ind_t start, ind_t end);
    ~Interval() {};
    ind_t GetStart() const;
    ind_t GetEnd() const;
    ind_t GetSize() const;
private:
    ind_t _start_ind;
    ind_t _end_ind;
};

class IndicesStorage {
public:
    IndicesStorage() {};
    IndicesStorage(Interval main_interval, ind_t parts);
    ~IndicesStorage() { delete _indices; };
    void Append(Interval new_interval);
    ind_t GetSize() const;
    Interval operator [] (ind_t pos) const;
    indices_status GetStatus();
    Interval GetBoundaries() const;
    bool GetRecall(Interval main_interval);
private:
    void Check();
    std::vector<class Interval>* _indices = new std::vector<class Interval>;
    indices_status _status = UNKNOWN;
    ind_t _size = 0;
    Interval _boundaries;
};

#endif /* interval_hpp */
