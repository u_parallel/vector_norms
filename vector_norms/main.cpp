#include <iostream>
#include "large_vector.hpp"

#define HELP "#################### OPERATION MANUAL ####################\n\
This program allows you to compare multithread and one-thread norm computation on your own sequences of float numbers from file and repeatedly generated \"1 2 3 4 5 6 7 8 9 0\" sequence. \n\
It will print some kind of a progress bar and overall time of computation.\n\n\
There are two types of norm: euqlidian and max. \n\
The program can take sequence from file with space and/or newline delimiter. \
\n\
List of keys:\n\
--help Shows this manual.\n\
--threads n Set number of threads to n. Default: 4.\n\
--file f_n Read vector from file f_n. Optional.\n\
--gen n Generate vector of n dimmensions. Default: 300000000. Activates if --file is not mentioned.\n\
--norm n Specifies norm to be computed.\n\
    e - euqlidian\n\
    m - max\n\
    b - both\n\
    Default: b\n\
"
void ArgRec(int argc,
            const char* argv[],
            std::string* filename,
            ind_t* threads,
            ind_t* gen,
            char* mode){
    if (argc == 1) {
        return;
    }
    for (int i = 1; i < argc - 1; i++) {
        if (strcmp(argv[i], "--file") == 0) {
            if (i + 1 < argc) {
                (*filename).assign(argv[i + 1]);
            }
        } else if (strcmp(argv[i], "--norm") == 0) {
            if (strstr(argv[i + 1], "e") != nullptr) {
                *mode = 'e';
            }
            if (strstr(argv[i + 1], "m") != nullptr) {
                *mode = 'm';
            }
        } else if (strcmp(argv[i], "--gen") == 0) {
            unsigned long long int temp_gen = strtoull (argv[i + 1], NULL, 10);
            if (temp_gen != 0) {
                *gen = (ind_t)temp_gen;
            }
        } else if (strcmp(argv[i], "--threads") == 0) {
            unsigned long long int temp_threads = strtoull (argv[i + 1], NULL, 10);
            if (temp_threads != 0) {
                *threads = (ind_t)temp_threads;
            }
        }
    }
}

int main(int argc, const char * argv[]) {
    
    norm_t norm;
    std::string filename;
    ind_t threads = 4;
    ind_t gen = 300000000;
    char mode = 'b';
    LargeVector<norm_t>* l_vec;
    
    try {
        if (argc == 2 && strcmp(argv[1], "--help") == 0) {
            std::cout << HELP;
        } else {
            ArgRec(argc, argv, &filename, &threads, &gen, &mode);
            
            if (filename.size() != 0) {
                std::cout << "Data will be gathered from file " << filename << ". ";
                l_vec = new LargeVector<norm_t>(filename);
            } else {
                std::cout << "Data will be generated in amount of " << gen << ". ";
                l_vec = new LargeVector<norm_t>(gen);
            }
            
            if (mode == 'e') {
                std::cout << "Euqlidian norm will be computed by ";
            } else if (mode == 'm') {
                std::cout << "Max norm will be computed by ";
            } else {
                std::cout << "Euqlidian and Max norms will be computed by ";
            }
            
            std::cout << threads << " threads.\n";
            
            if (mode == 'e' || mode == 'b') {
                norm = l_vec->GetEuqNorm(threads);
                std::cout << "Euq Norm is " << norm << "\n\n";
            }
            
            if (mode == 'm' || mode == 'b') {
                norm = l_vec->GetMaxNorm(threads);
                std::cout << "Max Norm is " << norm << "\n\n";
            }
            
            delete l_vec;
        }
    } catch (const std::runtime_error& re) {
        std::cout << "Runtime error: " << re.what() << std::endl;
    } catch (const std::exception& ex) {
        std::cout << "Error occurred: " << ex.what() << std::endl;
    } catch (...) {
        std::cout << "Unknown failure occurred. Possible memory corruption" << std::endl;
    }
    
    return 0;
}
