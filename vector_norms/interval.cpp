#include "interval.hpp"

Interval::Interval(ind_t start, ind_t end) {
    if (start > end) {
        std::string er = "start index > end index in ";
        er += __PRETTY_FUNCTION__;
        throw std::invalid_argument(er);
    }
    _start_ind = start;
    _end_ind = end;
}


/**
 Returns the start index of the interval
 
 @return _start_ind
 */
ind_t Interval::GetStart() const {
    return _start_ind;
}


/**
 Returns the end index of the interval
 
 @return _end_ind
 */
ind_t Interval::GetEnd() const {
    return _end_ind;
}

ind_t Interval::GetSize() const {
    return _end_ind - _start_ind + 1;
}


IndicesStorage::IndicesStorage(Interval main_interval, ind_t parts) {
    if (parts == 0) {
        std::string er = "parts is zero in ";
        er += __PRETTY_FUNCTION__;
        throw std::invalid_argument(er);
    }
    if (main_interval.GetSize() < parts) {
        parts = main_interval.GetSize();
    }
    
    ind_t interval_size = main_interval.GetSize() / parts;
    ind_t begin = main_interval.GetStart();
    
    for (ind_t i = 0; i < parts; i++) {
        Interval temp_indcs = Interval(begin + i * interval_size, begin + (i + 1) * interval_size - 1);
        if (i == parts - 1) {
            temp_indcs = Interval(begin + i * interval_size, main_interval.GetEnd());
        }
        _indices->push_back(temp_indcs);
    }
    
    _size = parts;
    _boundaries = main_interval;
}

void IndicesStorage::Append(Interval new_interval) {
    if (GetSize() != 0) {
        if (GetBoundaries().GetEnd() + 1 != new_interval.GetStart()) {
            std::string er = "new interval is not continuing existing boundaries in ";
            er += __PRETTY_FUNCTION__;
            throw std::invalid_argument(er);
        }
        _boundaries = Interval(GetBoundaries().GetStart(), new_interval.GetEnd());
    } else {
        _boundaries = new_interval;
    }
    
    _indices->push_back(new_interval);
    _size++;
}

ind_t IndicesStorage::GetSize() const {
    return _size;
}

Interval IndicesStorage::operator [] (ind_t pos) const {
    if (GetSize() <= pos) {
        std::string er = "pos is out of range in ";
        er += __PRETTY_FUNCTION__;
        throw std::invalid_argument(er);
    }
    return (*_indices)[pos];
}

void IndicesStorage::Check() {
    _status = BAD;
    if (GetSize() == 0) {
        return;
    }
    ind_t global_start = _boundaries.GetStart();
    ind_t global_end = _boundaries.GetEnd();
    ind_t current_end = global_start;
    for (typename std::vector<class Interval>::iterator it = _indices->begin(); it != _indices->end(); it++) {
        if (it->GetStart() < global_start) {
            return;
        }
        if (it->GetEnd() > global_end) {
            return;
        }
        if (current_end == global_start) {
            current_end = it->GetEnd();
            continue;
        } else if (current_end + 1 != it->GetStart()) {
            return;
        }
        current_end = it->GetEnd();
    }
    _status = OK;
}

indices_status IndicesStorage::GetStatus() {
    Check();
    return _status;
}

Interval IndicesStorage::GetBoundaries() const {
    return _boundaries;
}

bool IndicesStorage::GetRecall(Interval main_interval) {
    if (GetSize() == 0) {
        return false;
    }else if (GetStatus() == OK &&
        main_interval.GetStart() == GetBoundaries().GetStart() &&
        main_interval.GetEnd() == GetBoundaries().GetEnd()) {
        return true;
    } else {
        return false;
    }
    
}
